from engine.ecs import Entity
from engine import Manager

class Tileset(Entity):
  def __init__(self, tileset, tile_width, tile_height, scale=1):
    super().__init__()
    self.tileset = tileset
    self.all_tiles = {}
    self.tile_data = []
    self.tile_width = tile_width
    self.tile_height = tile_height
    self.scale = scale

    if isinstance(tileset, str):
      self.tileset = Manager.ASSETS.get_texture('{0}'.format(tileset), '{0}'.format(tileset))
    
    index = 0
    for y in range(0, self.tileset.get_height(), tile_height):
      for x in range(0, self.tileset.get_width(), tile_width):
        self.all_tiles[index] = self.tileset.subsurface((x, y, tile_width, tile_height))
        index += 1
      