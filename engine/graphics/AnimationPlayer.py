from engine.ecs import Component
from engine.graphics import Animation

class AnimationPlayer(Component):
  def __init__(self):
    super().__init__()
    self.animations = {}
    self.current_animation = None
  
  def get_animation(self, name):
    if name in self.animations:
      return self.animations[name]
    else:
      return None
  
  def remove(self, name):
    if name in self.animations:
      animation = self.animations[name]

      if self.current_animation and self.current_animation == animation:
        self.stop()
        del self.animations[name]
  
  def add(self, name, textures, fps = 16, loop = True):
    animation = Animation(self, name, textures, fps, loop)
    self.animations[name] = animation

    return animation

  def swap_to(self, name):
    if name in self.animations:
      self.current_animation = self.animations[name]
    else:
      print('No animation found for {0}'.format(name))
  
  def play(self, name):
    if name in self.animations:
      self.current_animation = self.animations[name]
      texture = self.current_animation._play()

      if self.parent:
        self.parent.texture = texture
    else:
      print('No animation found for {0}'.format(name))
  
  def stop(self):
    if self.current_animation:
      self.current_animation._stop()

  def pause(self):
    if self.current_animation:
      self.current_animation._pause()

  def resume(self):
    if self.current_animation:
      self.current_animation._resume()
      
  def on_render(self):
    if self.current_animation:
      texture = self.current_animation._update()
      if texture:
        if self.parent:
          self.parent.texture = texture
