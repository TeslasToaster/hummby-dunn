from engine.ecs import Entity
from engine import Manager

class Sprite(Entity):
  def __init__(self, texture = None, scale_x=1, scale_y=1):
    super().__init__()
    self.texture = texture

    if isinstance(texture, str):
      try:
        self.texture = Manager.ASSETS.get_texture('{0}'.format(texture), '{0}'.format(texture))
      except:
        self.texture = Manager.ASSETS.get_texture('no_image_found', 'engine/builtins/assets/gfx/noImageFound.png')

    self.scale_x = scale_x
    self.scale_y = scale_y