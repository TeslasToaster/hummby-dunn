class ObjectPool():
  def __init__(self, class_type, capacity = 100):
    self._class_type = class_type
    self._capacity = capacity
    self._released = []
  
  def release_all(self):
    self._released = []
  
  def release(self, obj):
    if len(self._released) < self._capacity:
      self._released.append(obj)
  
  def get(self):
    if len(self._released) > 0:
      return self._released.pop()
    else:
      return self._class_type()