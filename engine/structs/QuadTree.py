from pygame import Rect
from engine import Manager

MAX_ENTITIES = 1
MAX_LEVELS = 2

class QuadTree(object):
  def __init__(self, level = 0, bounds = None):
    super().__init__()
    self.level = level
    self._bounds = bounds
    if not self._bounds:
      self._bounds = Rect(0, 0, Manager.VIEWPORT.width, Manager.VIEWPORT.height)
    self.objects = []
    self._nodes = [None for i in range(4)]
  
  def clear(self):
    self.objects = []

    for i in range(0, len(self._nodes)):
      if self._nodes[i]:
        self._nodes[i].clear()
        self._nodes[i] = None
  
  def split(self):
    sub_width = int(self._bounds.width / 2)
    sub_height = int(self._bounds.height / 2)
    x = int(self._bounds.x)
    y = int(self._bounds.y)

    self._nodes[0] = QuadTree(self.level+1, Rect(x + sub_width, y, sub_width, sub_height))
    self._nodes[1] = QuadTree(self.level+1, Rect(x, y, sub_width, sub_height))
    self._nodes[2] = QuadTree(self.level+1, Rect(x, y + sub_height, sub_width, sub_height))
    self._nodes[3] = QuadTree(self.level+1, Rect(x + sub_width, y + sub_height, sub_width, sub_height))
  
  def get_index(self, entity):
    # Shouldn't this be only entities with the colision component?
    # entity_bounds = entity.bounds
    # WHY THE HELL DOESNT THE IMPORT OF BOXCOLLIDER WORK?!
    entity_bounds = entity.get_component('Body').bounds

    index = -1

    vertical_mid = self._bounds.x + (self._bounds.width / 2)
    horizontal_mid = self._bounds.y + (self._bounds.height / 2)

    is_top = (entity_bounds.y < horizontal_mid and entity_bounds.y + entity_bounds.height < horizontal_mid)
    is_bottom = (entity_bounds.y > horizontal_mid)

    is_left = (entity_bounds.x < vertical_mid and entity_bounds.x + entity_bounds.width < vertical_mid)
    is_right = (entity_bounds.x >= vertical_mid)

    index = 0 if is_right and is_top else index
    index = 1 if is_left and is_top else index
    index = 2 if is_left and is_bottom else index
    index = 3 if is_right and is_bottom else index
    
    return index
  
  def insert(self, entity):
    if self._nodes[0] != None:
      index = self.get_index(entity)

      if index != -1:
        self._nodes[index].insert(entity)
        return
        
    self.objects.append(entity)

    if len(self.objects) >= MAX_ENTITIES and self.level < MAX_LEVELS:
      if self._nodes[0] == None:
        self.split()
      
      i = 0
      while i < len(self.objects):
        index = self.get_index(self.objects[i])
        if index != -1:
          self._nodes[index].insert(self.objects.pop(i))
        else:
          i += 1
  
  def retrieve(self, entity):
    entities = []
    index = self.get_index(entity)
    if index != -1 and self._nodes[0] is not None:
      entities += self._nodes[index].retrieve(entity)

    entities += self.objects

    if entity in entities:
      entities.remove(entity)

    return entities