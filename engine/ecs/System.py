from engine.events import Emitter

class System(Emitter):
  def on_pause(self):
    pass

  def on_resume(self):
    pass

  def on_update(self, delta):
    pass
  
  def after_update(self):
    pass

  def on_render(self):
    pass

  def on_added(self, entity):
    pass

  def on_removed(self, entity):
    pass