from pygame import Rect
from engine.events import Emitter
from engine.ecs.Component import Component

ID = 0

class Entity(Emitter):
  def __init__(self):
    super().__init__()
    global ID
    self.id = ID
    ID += 1
    self.parent = None
    self.bounds = Rect(0,0,0,0)
    self.children = []
    self.components = []
  
  def on_added(self, parent):
    pass

  def on_removed(self):
    pass

  def on_update(self):
    pass

  def update(self, delta):
    self.on_update()

    for component in self.components:
      component.on_update()
  
    for child in self.children:
      child.update(delta)
  
  def add(self, item):
    if isinstance(item, Entity):
      self.add_child(item)
    if isinstance(item, Component):
      self.add_component(item)

  def add_component(self, component):
    if component.parent != self:
      component.on_removed(component.parent)
      component._remove_from_parent()
      component.parent = self

      self.components.append(component)
      component.on_added(self)

    return component
  
  def add_child(self, child):
    if child.parent == self:
      print('cant add')
      return child
    child._set_parent(self)
    self.children.append(child)
    child.on_added(self)

    return child
  
  def _set_parent(self, parent):
    self.parent = parent

  def remove_child(self, child):
    if child in self.children:
      self.children.remove(child)
      child.on_removed()
      return child

  def remove_component(self, component):
    if component in self.components:
      self.components.remove(component)
      component.on_removed(self)
      return component
  
  def get_component(self, component_class):
    if isinstance(component_class, str):
      for component in self.components:
        if component_class in component.__module__:
          return component
    else:
      for component in self.components:
        if component.__module__ == component_class.__name__:
          return component

    return None

  def for_each(node, action):
    action(node)
    for child in node.children:
      Entity.for_each(child, action)
  
  def find_by_id(node, id):
    if node.id == id:
      return node
    
    for child in node.children:
      result = Entity.find_by_id(child, id)
      if result:
        return result

    return None