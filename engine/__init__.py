import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
import pygame

pygame.init()

from pygame import Rect
from engine.logger import *
from engine.assets import *
from engine.audio import *
from engine.builtins import *
from engine.display import *
from engine.ecs import *
from engine.events import *
from engine.graphics import *
from engine.input import *
from engine.physics import *
from engine.render import *
from engine.states import *
from engine.structs import *

from engine.core import Engine

engine = Engine()

def start(runner, title, width=800, height=600, target_fps=30):
  engine.init(runner, title, width, height, target_fps)
  engine.start()