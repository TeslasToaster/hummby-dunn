import sys
import traceback
import logging
import pygame
from engine.display import Stage, Viewport
from engine.events import Emitter
from engine.input import Input
from engine.physics import Arcade
from engine.render import Renderer
from engine import Manager

DEFAULT_FPS = 30

class Engine(Emitter):
  def __init__(self):
    super().__init__()
    Manager.ENGINE = self
    self._root_class = None
    self._systems = [Input, Arcade]
    self._stage = Stage()
    self._viewport = None
    self._renderer = None
    self._target_fps = DEFAULT_FPS

    self.running = True
    self._clock = pygame.time.Clock()
  
  def init(self, rootNode, title, width, height, target_fps=DEFAULT_FPS):
    self._root_class = rootNode
    self._viewport = Viewport(title, width, height)
    self._renderer = Renderer()
    Manager.VIEWPORT = self._viewport
    self._target_fps = target_fps
    self._init_systems()

    self._stage.add_child(self._root_class())
  
  def _init_systems(self):
    for i in range(0, len(self._systems)):
      self._systems[i] = self._systems[i]()

  def add_system(self, system):
    self._systems.append(system)
    system.on_added(self)
  
  def start(self):
    logging.info('Engine started successfully.')
    self._update()
  
  def _update(self):
    delta = 0
    try:
      while self.running:
        self._viewport.before_update()

        for system in self._systems:
          system.on_update(delta)

        self._stage.update(delta)

        for system in self._systems:
          system.after_update()

        for system in self._systems:
          system.on_render()

        self._renderer.on_render(self._stage)

        self._viewport.update()
        delta = self._clock.tick(self._target_fps)

    except KeyboardInterrupt as e:
      logging.info('Force Closing Engine.')
    except Exception as e:
      logging.error(e)
      traceback.print_exc()
    finally:
      logging.info('Closing Engine.')
      pygame.quit()
      quit()
    
  def stop(self):
    self.running = False
