import pygame
from engine.events import Emitter

class Viewport(Emitter):
  def __init__(self, window_name, width, height):
    super().__init__()
    self._window_name = window_name
    self._default_background_color = (56, 63, 88)
    self._background_color = self._default_background_color
    self.display = pygame.display.set_mode((width, height))
    self.width = self.display.get_width()
    self.height = self.display.get_height()

    self._init()
    
  def _init(self):
    pygame.display.set_caption('{0}'.format(self._window_name))
  
  def before_update(self):
    self.display.fill(self._background_color)

  def update(self):
    pygame.display.update()