from pygame.image import load
from engine import Manager
from engine.events import Emitter

class AssetManager(Emitter):
  def __init__(self):
    super().__init__()
    Manager.ASSETS = self
    self.assets = {}
  
  def clear_loaded(self):
    self.assets = {}
  
  def _cache(self, name, data):
    self.assets[name] = data
  
  def _get_cache(self, name):
    return self.assets[name]
  
  def _is_cached(self, name):
    return name in self.assets

  def get_texture(self, name, path, internal=False):
    if self._is_cached(name):
      return self._get_cache(name)

    texture = load(path).convert_alpha()
    self._cache(name, texture)
    return texture

  def get_texture_array(self, name, path, tile_width, tile_height, x_step=0, y_step=0, internal=False):
    if self._is_cached(name):
      return self._get_cache(name)

    base_texture = self.get_texture(name, path, internal)
    textures = []
    for y in range(0, base_texture.get_height(), tile_height + y_step):
      for x in range(0, base_texture.get_width(), tile_width + x_step):
        textures.append(base_texture.subsurface((x, y, tile_width, tile_height)))

    self._cache(name, textures)
    return textures

  def get_texture_subsurface(self, name, path, x, y, tile_width, tile_height, internal=False):
    if self._is_cached(name):
      return self._get_cache(name)

    base_texture = load(path).convert_alpha()
    base_texture = base_texture.subsurface((x, y, tile_width, tile_height))

    self._cache(name, base_texture)
    return base_texture

  def get_sound(self, name, internal=False):
    print('sound', name, internal)