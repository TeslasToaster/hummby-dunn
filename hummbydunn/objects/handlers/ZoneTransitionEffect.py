from engine import COMPLETE
from hummbydunn.objects.wrappers.ScaledSprite import ScaledSprite
from hummbydunn import config

max_height_scale = 70

class ZoneTransitionEffect(ScaledSprite):
  def __init__(self, transition_down=True):
    super().__init__('hummbydunn/assets/gfx/blank.png')
    self.speed = 3
    self.desc = transition_down

    self.scale_x = 2
    self.scale_y = max_height_scale if self.desc else 0

  def on_update(self):
    if self.desc:
      self.scale_y -= self.speed
      if self.scale_y < 0:
        self.parent.remove_child(self)
    else:
      if self.scale_y < max_height_scale:
        self.scale_y += self.speed
      if self.scale_y > max_height_scale:
        self.emit(COMPLETE)