from engine import MSG_COLLISION, NORTH, SOUTH, Body
from hummbydunn import config
from hummbydunn.objects.wrappers.Collidable import Collidable
from hummbydunn.objects.player.Player import Player

class Teleporter(Collidable):
  def __init__(self, to_zone, trigger_direction, to_x, to_y, x=0, y=0, vx=0, vy=0, width=config.SCALED_TILE_WIDTH/2, height=config.SCALED_TILE_WIDTH, offset_x=0, offset_y=0):
    super().__init__(None, x, y, width, height, offset_x, offset_y, True)
    # super().__init__('hummbydunn/assets/gfx/noImageFound.png', x, y, width, height)
    self._to_zone = to_zone
    self.to_x = to_x
    self.to_y = to_y
    self.trigger_direction = trigger_direction
    self.on(MSG_COLLISION, self._collide)
  
  def _collide(self, *args):
    collider = args[1]
    if isinstance(collider, Player):
      body = collider.get_component('Body')
      if body.direction == self.trigger_direction:
        self.parent.parent.emit('transition', self)
        self.parent.remove_child(self)
  
  def _destroy(self, *args):
    self.parent.remove_child(self)