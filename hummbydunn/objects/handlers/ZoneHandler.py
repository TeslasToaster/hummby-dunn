from engine import Entity, MSG_OUT_OF_BOUNDS, COMPLETE
from hummbydunn import config
from hummbydunn.objects.player.Player import Player, STARTING_X_COORD, STARTING_Y_COORD
from hummbydunn.objects.world import (
  Dungeon1,
  Dungeon1Top,
  Dungeon2,
  Dungeon2Top,
  Dungeon3,
  Dungeon3Top,
  House,
  HouseTop,
  Overworld,
  OverworldTop,
  TRANSITION
)
from hummbydunn.objects.handlers.ZoneTransitionEffect import ZoneTransitionEffect

SIZE = 16
SCALED = SIZE * config.SCALE

OVERWORLD_ID = "overworld"
HOUSE_ID = "house"
DUNGEON1_ID = "dungeon_1"
DUNGEON2_ID = "dungeon_2"
DUNGEON3_ID = "dungeon_3"

layers = {
  OVERWORLD_ID: {
    "bg": Overworld,
    "fg": OverworldTop,
  },
  HOUSE_ID: {
    "bg": House,
    "fg": HouseTop,
  },
  DUNGEON1_ID: {
    "bg": Dungeon1,
    "fg": Dungeon1Top,
  },
  DUNGEON2_ID: {
    "bg": Dungeon2,
    "fg": Dungeon2Top,
  },
  DUNGEON3_ID: {
    "bg": Dungeon3,
    "fg": Dungeon3Top,
  },
}


class ZoneHandler(Entity):
  def __init__(self):
    super().__init__()

    self.player = Player(STARTING_X_COORD, STARTING_Y_COORD)

    self.dungeon_level = 0

    self.add_child(Overworld())
    self.add_child(self.player)
    self.add_child(OverworldTop())
    # self.add_child(Dungeon1())
    # self.add_child(self.player)
    # self.add_child(Dungeon1Top())
    # self.add_child(ZoneTransitionEffect())
    self.on('transition', self._transition)

  def player_transition(self, teleporter):
    self.player.bounds.x = teleporter.to_x
    self.player.bounds.y = teleporter.to_y
    to_zone = layers[teleporter._to_zone]["bg"]()
    to_zone_fg = layers[teleporter._to_zone]["fg"]()

    to_zone.parent = self
    self.children[0] = to_zone
    self.children[2] = to_zone_fg
  
  def _transition(self, *args):
    teleporter = args[1]
    self.player_transition(teleporter)

  def transition(self, *args):
    transitioner = ZoneTransitionEffect(False)
    self.add_child(transitioner)
    transitioner.on(COMPLETE, lambda _: self.on_out_of_bounds(*args))