from hummbydunn.objects.wrappers.ScaledSprite import ScaledSprite
from hummbydunn import config

distance_trigger = 5

class PlayerIndicator(ScaledSprite):
  def __init__(self):
    super().__init__('hummbydunn/assets/gfx/spritesheets/player_indicator.png')
    self.scale_x = config.SCALE / 2
    self.scale_y = config.SCALE / 2

    self.speed = 1
    self.asc = True
    self.count = 0

  def on_update(self):
    player = self.parent
    self.count += -self.speed if self.asc else self.speed

    if self.asc:
      if self.bounds.y < (player.bounds.y - (config.TILE_SIZE)) - distance_trigger:
        self.asc = False
    else:
      if self.bounds.y > (player.bounds.y - (config.TILE_SIZE)) + distance_trigger:
        self.asc = True

    self.bounds.x = player.bounds.x + config.TILE_SIZE + (self.texture.get_width() / 2)
    self.bounds.y = player.bounds.y - (config.TILE_SIZE) + self.count