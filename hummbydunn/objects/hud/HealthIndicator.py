from hummbydunn.objects.wrappers.ScaledSprite import ScaledSprite
from hummbydunn import config

class HealthIndicator(ScaledSprite):
  def __init__(self):
    super().__init__('hummbydunn/assets/gfx/health.png')
    self.scale_x = config.SCALE / 2
    self.scale_y = config.SCALE / 4

  def on_update(self):
    entity = self.parent
    if entity.current_health == entity.max_health:
      self.scale_x = 0
      return
    self.bounds.x = entity.bounds.x
    self.scale_x = entity.current_health
    self.bounds.y = entity.bounds.y - (config.TILE_SIZE)