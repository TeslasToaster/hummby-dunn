from hummbydunn.objects.world.Dungeon1 import Dungeon1, Dungeon1Top
from hummbydunn.objects.world.Dungeon2 import Dungeon2, Dungeon2Top
from hummbydunn.objects.world.Dungeon3 import Dungeon3, Dungeon3Top
from hummbydunn.objects.world.House import House, HouseTop
from hummbydunn.objects.world.Overworld import Overworld, OverworldTop

TRANSITION = 'transition'