from hummbydunn.objects.characters.CatQueen import CatQueen
from hummbydunn.objects.characters.Dumbdumb import Dumbdumb
from hummbydunn.objects.characters.Guard import Guard
from hummbydunn.objects.characters.HouseCharacter1 import HouseCharacter1
from hummbydunn.objects.characters.Oldie import Oldie
from hummbydunn.objects.characters.theTrio import Man1B, Man2J, Man3E
from hummbydunn.objects.characters.Townfolk import Townfolk
from hummbydunn.objects.characters.Wizard import Wizard