from engine import Manager, AnimationPlayer, NORTH, SOUTH, EAST, WEST
from hummbydunn import config
from hummbydunn.objects.wrappers.BaseCharacter import BaseCharacter
from hummbydunn.objects.hud.PlayerIndicator import PlayerIndicator

SIZE = 16
SCALED = SIZE * config.SCALE

class Oldie(BaseCharacter):
  def __init__(self, x=0, y=0):
    super().__init__('hummbydunn/assets/gfx/tiles/dungeon/tile_0100.png', x, y, SCALED, SCALED)

    self.bounds.width = config.SCALED_TILE_WIDTH
    self.bounds.height = config.SCALED_TILE_WIDTH

    self.current_health = 2
    self.max_health = 2

    body = self.get_component('Body')
    body.direction = SOUTH