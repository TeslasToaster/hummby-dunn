from engine import Manager, AnimationPlayer, NORTH, SOUTH, EAST, WEST
from hummbydunn import config
from hummbydunn.objects.wrappers.BaseCharacter import BaseCharacter
from hummbydunn.objects.hud.PlayerIndicator import PlayerIndicator

SIZE = 16
SCALED = SIZE * config.SCALE

class CatQueen(BaseCharacter):
  def __init__(self, x=0, y=0):
    super().__init__('hummbydunn/assets/gfx/tiles/urban/tile_0429.png', x, y, SCALED, SCALED)

    self.bounds.width = config.SCALED_TILE_WIDTH
    self.bounds.height = config.SCALED_TILE_WIDTH

    self.current_health = 30
    self.max_health = 30

    body = self.get_component('Body')
    body.direction = SOUTH