from engine import MSG_OUT_OF_BOUNDS, Manager
from hummbydunn.objects.wrappers.Collidable import Collidable

class BaseProjectile(Collidable):
  SPEED = 1
  def __init__(self, x=0, y=0, vx=0, vy=0, width=0, height=0):
    super().__init__(None, x, y, width, height)
    body = self.get_component('Body')
    body.velocity.x = vx
    body.velocity.y = vy
    self.damage = 1
    self.on(MSG_OUT_OF_BOUNDS, self._destroy)

  def set_damage(self, value):
    self.damage = value

  def on_added(self, parent):
    pass
  
  def _destroy(self, *args):
    self.parent.remove_child(self)