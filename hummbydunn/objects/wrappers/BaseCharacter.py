import logging
from engine import MSG_COLLISION
from hummbydunn.objects.player.Player import Player
from hummbydunn.objects.hud.HealthIndicator import HealthIndicator
from hummbydunn.objects.wrappers.Collidable import Collidable
from hummbydunn.objects.wrappers.BaseProjectile import BaseProjectile

class BaseCharacter(Collidable):
  def __init__(self, texture=None, x=0, y=0, width=0, height=0):
    super().__init__(texture, x, y, width, height)
    self.on(MSG_COLLISION, self._collide)
    self.indicator = HealthIndicator()
    self.add(self.indicator)

    self.current_health = 5
    self.max_health = 5
  
  def on_player_touch(self, player):
    print("{0} doesn't implement on_player_touch(self, player)".format(self.__class__.__name__))
  
  
  def on_projectile_touch(self, item):
    item.parent.remove_child(item)
    self.current_health -= 1
    if self.current_health < 0:
      self.current_health = 0
  
  def _collide(self, *args):
    collider = args[1]
    if isinstance(collider, Player):
      self.on_player_touch(collider)
    if isinstance(collider, BaseProjectile):
      self.on_projectile_touch(collider)