from engine import Manager, AnimationPlayer
from hummbydunn import config
from hummbydunn.objects.wrappers.BaseItem import BaseItem

SIZE = 16
SCALED = SIZE * config.SCALE

STARTING_X_TILE = 22
STARTING_Y_TILE = 13

STARTING_X_COORD = config.SCALED_TILE_WIDTH * STARTING_X_TILE
STARTING_Y_COORD = config.SCALED_TILE_WIDTH * STARTING_Y_TILE

class OverworldArmor(BaseItem):
  def __init__(self, x=0, y=0):
    super().__init__(STARTING_X_COORD, STARTING_Y_COORD, SCALED, SCALED)

    self.texture = Manager.ASSETS.get_texture('item_armor', 'hummbydunn/assets/gfx/spritesheets/item_armor.png')

  def on_player_touch(self, player):
    print('Increase player defense')