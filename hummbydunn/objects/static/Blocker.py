from engine import MSG_COLLISION, NORTH, SOUTH, Body
from hummbydunn import config
from hummbydunn.objects.wrappers.Collidable import Collidable

class Blocker(Collidable):
  def __init__(self, x=0, y=0, width=config.SCALED_TILE_WIDTH, height=config.SCALED_TILE_WIDTH):
    super().__init__(None, x, y, width, height)