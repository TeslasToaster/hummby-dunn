from engine import Manager, AnimationPlayer, NORTH, SOUTH, EAST, WEST, MSG_COLLISION
from hummbydunn import config
from hummbydunn.objects.wrappers.Collidable import Collidable
from hummbydunn.objects.hud.PlayerIndicator import PlayerIndicator
from hummbydunn.objects.projectiles.Projectile import Projectile, NORMAL, MAGICAL
from hummbydunn.objects.static.Blocker import Blocker

SIZE = 16
SCALED = SIZE * config.SCALE

PLAYER_SPEED = 5

STARTING_X_TILE = 8
STARTING_Y_TILE = 5


STARTING_X_COORD = SCALED * STARTING_X_TILE
STARTING_Y_COORD = SCALED * STARTING_Y_TILE

class Player(Collidable):
  def __init__(self, x=0, y=0):
    super().__init__(None, x, y, SCALED * 3/4, SCALED / 2, offset_x=SCALED/12, offset_y=SCALED / 2)
    self._holding = {
      Manager.INPUT.UP: False,
      Manager.INPUT.LEFT: False,
      Manager.INPUT.DOWN: False,
      Manager.INPUT.RIGHT: False,
    }

    self.anim = AnimationPlayer()
    self.bounds.width = config.SCALED_TILE_WIDTH
    self.bounds.height = config.SCALED_TILE_WIDTH

    self.indicator = PlayerIndicator()

    self.inventory = {
      Manager.INPUT.A: Projectile,
      Manager.INPUT.B: None,
    }
  
    textures = Manager.ASSETS.get_texture_array('player_up', 'hummbydunn/assets/gfx/spritesheets/player_anim_walk_up.png', 16, 16)
    self.anim.add('walk_up', textures, fps=3, loop=True)

    textures = Manager.ASSETS.get_texture_array('player_left', 'hummbydunn/assets/gfx/spritesheets/player_anim_walk_left.png', 16, 16)
    self.anim.add('walk_left', textures, fps=3, loop=True)

    textures = Manager.ASSETS.get_texture_array('player_down', 'hummbydunn/assets/gfx/spritesheets/player_anim_walk_down.png', 16, 16)
    self.anim.add('walk_down', textures, fps=3, loop=True)

    textures = Manager.ASSETS.get_texture_array('player_right', 'hummbydunn/assets/gfx/spritesheets/player_anim_walk_right.png', 16, 16)
    self.anim.add('walk_right', textures, fps=3, loop=True)

    self.add_component(self.anim)
    self.anim.play('walk_down')

    body = self.get_component('Body')
    body.direction = SOUTH

    self.add(self.indicator)

    Manager.INPUT.on(Manager.INPUT.KEY_DOWN, self.on_input_press)
    Manager.INPUT.on(Manager.INPUT.KEY_UP, self.on_input_release)
    self.on(MSG_COLLISION, self._collide)
  
  def on_blocker_touch(self, blocker):
    pass
  
  def _collide(self, *args):
    collider = args[1]
    if isinstance(collider, Blocker):
      self.on_blocker_touch(collider)
  
  def on_update(self):
    body = self.get_component('Body')
    horizontal_movement = 0
    vertical_movement = 0
    (col_t, col_l, col_b, col_r) = body.collision_map
    current_tile_section = (body.bounds.x / (config.SCALED_TILE_WIDTH / 2))
    left_closest_half = int((body.bounds.x - config.SCALED_TILE_WIDTH / 2) / (config.SCALED_TILE_WIDTH / 2))
    right_closest_half = int((body.bounds.x + config.SCALED_TILE_WIDTH / 2) / (config.SCALED_TILE_WIDTH / 2))
    needs_to_snap_vertical = body.bounds.x % (config.SCALED_TILE_WIDTH / 2) != 0

    percentage_go_right = abs(current_tile_section - left_closest_half) - 1
    percentage_go_left = abs(current_tile_section - right_closest_half)

    if self._holding[Manager.INPUT.LEFT]:
      if not col_l:
        horizontal_movement += -PLAYER_SPEED
      body.direction = WEST

    if self._holding[Manager.INPUT.RIGHT]:
      if not col_r:
        horizontal_movement += PLAYER_SPEED
      body.direction = EAST

    if self._holding[Manager.INPUT.UP]:
      if not col_t:
        horizontal_movement = 0
        vertical_movement += -PLAYER_SPEED
      body.direction = NORTH
    
    if self._holding[Manager.INPUT.DOWN]:
      if not col_b:
        horizontal_movement = 0
        vertical_movement += PLAYER_SPEED
      body.direction = SOUTH

    if vertical_movement == 0 and horizontal_movement == 0:
      self.anim.pause()
    else:
      self.anim.resume()

    if body.direction == NORTH:
      self.anim.swap_to('walk_up')
    if body.direction == WEST:
      self.anim.swap_to('walk_left')
    if body.direction == SOUTH:
      self.anim.swap_to('walk_down')
    if body.direction == EAST:
      self.anim.swap_to('walk_right')

    body.velocity.x = horizontal_movement
    body.velocity.y = vertical_movement
  
  def on_input_press(self, *args):
    key = args[1]
    self._holding[key] = True
    
    if key == Manager.INPUT.A:
      self.on_a_press()
    if key == Manager.INPUT.B:
      self.on_b_press()
    
  def on_input_release(self, *args):
    self._holding[args[1]] = False
  
  def on_a_press(self):
    body = self.get_component('Body')
    item = self.inventory[Manager.INPUT.A]
    if item is None:
      return
    if body.direction == NORTH:
      item = item(NORTH, NORMAL, body.bounds.x, body.bounds.y, 0, -item.SPEED)
    if body.direction == WEST:
      item = item(WEST, NORMAL, body.bounds.x, body.bounds.y, -item.SPEED, 0)
    if body.direction == SOUTH:
      item = item(SOUTH, NORMAL, body.bounds.x, body.bounds.y, 0, item.SPEED)
    if body.direction == EAST:
      item = item(EAST, NORMAL, body.bounds.x, body.bounds.y, item.SPEED, 0)

    self.add(item)

  def on_b_press(self):
    body = self.get_component('Body')
    item = self.inventory[Manager.INPUT.B]
    if item is None:
      return
    if body.direction == NORTH:
      item = item(NORTH, NORMAL, 0, -item.SPEED)
    if body.direction == WEST:
      item = item(WEST, MAGICAL, -item.SPEED, 0)
    if body.direction == SOUTH:
      item = item(SOUTH, NORMAL, 0, item.SPEED)
    if body.direction == EAST:
      item = item(EAST, MAGICAL, item.SPEED, 0)

    item.bounds.x = self.bounds.x
    item.bounds.y = self.bounds.y

    self.add(item)