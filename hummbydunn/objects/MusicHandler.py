import os
from engine import Entity, Manager
from hummbydunn import config
from pygame.mixer import Sound

class MusicHandler(Entity):
  def __init__(self):
    super().__init__()
    path = os.path.dirname(os.path.abspath(__file__))
    self.theme = Sound('{0}/../assets/sfx/music/theme.wav'.format(path))
    self.theme.play(loops=-1)