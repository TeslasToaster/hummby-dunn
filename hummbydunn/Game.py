from engine import Entity
from hummbydunn.objects import ZoneHandler, OverworldArmor, MusicHandler

class Game(Entity):
  def __init__(self):
    super().__init__()
    handler = ZoneHandler()
    
    self.add_child(handler)
    self.add_child(MusicHandler())

