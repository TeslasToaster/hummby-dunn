from hummbydunn import Game
from hummbydunn.config import SCALED_WINDOW_DIMENSIONS
from engine import start

if __name__ == '__main__':
  start(Game, "HummbyDunn", SCALED_WINDOW_DIMENSIONS[0], SCALED_WINDOW_DIMENSIONS[1], 60)